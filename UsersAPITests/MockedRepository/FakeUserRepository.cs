﻿using System;
using System.Collections.Generic;
using System.Linq;
using UsersAPI.Data.Repositories.Interfaces;
using UsersAPI.Entities;
using UsersAPI.Helpers;

namespace UsersAPITests.MockedRepository
{
    public class FakeUserRepository : IUserRepository
    {
        private readonly List<User> _users;

        public FakeUserRepository()
        {
            _users = new List<User>()
            {
                new User() {
                    Id = Guid.NewGuid(),
                    FirstName = "Everton",
                    LastName ="Medrado",
                    BirthDate = RandomDate.RandomDay(),
                    ShortInfo = "Hello! I'm a test user",
                    RegisterDate = DateTime.Now,
                    IsActive = true
                },
                new User() {
                    Id = Guid.NewGuid(),
                    FirstName = "Marcos",
                    LastName ="Henrique",
                    BirthDate = RandomDate.RandomDay(),
                    ShortInfo = "Hello! I'm a test user",
                    RegisterDate = DateTime.Now,
                    IsActive = false
                },
                 new User() {
                    Id = Guid.NewGuid(),
                    FirstName = "Ingresse",
                    LastName ="dotCom",
                    BirthDate = RandomDate.RandomDay(),
                    ShortInfo = "Hello! I'm a test user",
                    RegisterDate = DateTime.Now,
                    IsActive = false
                }
            };
        }

        public void Add(User obj)
        {
            _users.Add(obj);
        }

        public IEnumerable<User> GetAll()
        {
            return _users;
        }

        public User GetById(Guid id)
        {
            return _users.Where(u => u.Id == id).FirstOrDefault();
        }

        public void Remove(User obj)
        {
            _users.Remove(obj);
        }

        public void Update(User obj)
        {
            var updatedUser = _users.Where(u => u.Id == obj.Id).FirstOrDefault();

            _users.Remove(updatedUser);

            updatedUser.FirstName = obj.FirstName;
            updatedUser.LastName = obj.LastName;
            updatedUser.BirthDate = obj.BirthDate;
            updatedUser.ShortInfo = obj.ShortInfo;
            updatedUser.IsActive = obj.IsActive;

            _users.Add(updatedUser);
            var totalUsers = _users.Count();
        }
        public void Commit()
        {
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
