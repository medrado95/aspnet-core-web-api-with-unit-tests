﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UsersAPI.Controllers;
using UsersAPI.Data.Repositories.Interfaces;
using UsersAPI.Entities;
using UsersAPI.Helpers;
using UsersAPITests.MockedRepository;
using Xunit;

namespace UsersAPITests.ControllersTests
{
    public class UserControllerTests
    {
        UsersController _controller;
        IUserRepository _userRepository;
        private Mock<IDistributedCache> _distributedCache;

        public UserControllerTests()
        {
            _distributedCache = new Mock<IDistributedCache>();
            _userRepository = new FakeUserRepository();
            _controller = new UsersController(_userRepository, _distributedCache.Object);

        }

        // START TESTING GET METHOD : GET fv 2.1.402-sdk
        [Fact]
        public void Get_Returns_Ok()
        {
            var okResult = _controller.GetAll();
            Assert.IsType<OkObjectResult>(okResult.Result);
        }
        //[Fact]
        //public void Get_Returns_NotModified()
        //{
        //    var users = _userRepository.GetAll();
        //    var randomUser = users.Where(u => u.IsActive == true).FirstOrDefault();

        //    _controller.GetById(randomUser.Id);
        //    var getResult = _controller.GetById(randomUser.Id);

        //}

        [Fact]
        public void Get_ReturnAllItems()
        {
            var okResult = _controller.GetAll().Result as OkObjectResult;
            var items = Assert.IsType<List<User>>(okResult.Value);
            Assert.Equal(3, items.Count);
        }
        //END OF GET TESTS

        //START TESTING GETBYID METHOD : GET
        [Fact]
        public void GetById_UnknownGuid_ReturnsNotFoundResult()
        {
            var getResult = _controller.GetById(Guid.NewGuid());

            Assert.IsType<NotFoundResult>(getResult.Result);
        }

        [Fact]
        public void GetById_ExistingGuid_ReturnsOk()
        {
            var users = _userRepository.GetAll();
            var randomUser = users.Where(u => u.IsActive == true).FirstOrDefault();

            var guid = randomUser.Id;
            var getResult = _controller.GetById(guid);

            Assert.IsType<OkObjectResult>(getResult.Result);
        }

        [Fact]
        public void GetById_ExistingGuid_ReturnsRightItem()
        {
            var users = _userRepository.GetAll();
            var randomUser = users.Where(u => u.IsActive == true).FirstOrDefault();

            var okResult = _controller.GetById(randomUser.Id).Result as OkObjectResult;

            Assert.IsType<string>(okResult.Value);
            Assert.Equal(JsonConvert.SerializeObject(randomUser), okResult.Value);
        }
        //END TESTING GETBYID METHOD 

        // START TESTING CREATE METHOD : POST
        [Fact]
        public void Create_InvalidObjectPassed_ReturnsBadRequest()
        {
            var nameMissing = new User()
            {
                LastName = "Medrado",
                BirthDate = RandomDate.RandomDay(),
                ShortInfo = "Hello! I'm a test user",
                RegisterDate = DateTime.Now,
                IsActive = true
            };

            _controller.ModelState.AddModelError("Name", "Required");

            var badResponse = _controller.Create(nameMissing);

            Assert.IsType<BadRequestResult>(badResponse);
        }


        [Fact]
        public void Create_ValidObjectPassed_ReturnsCreatedResponse()
        {
            User testUser = new User()
            {
                FirstName = "Everton",
                LastName = "Medrado",
                BirthDate = RandomDate.RandomDay(),
                ShortInfo = "Hello! I'm a test user",
                RegisterDate = DateTime.Now,
                IsActive = true
            };

            var createdResponse = _controller.Create(testUser);

            Assert.IsType<CreatedAtRouteResult>(createdResponse);
        }
        //END TESTING CREATE METHOD 

        //START TESTING PUT METHOD : PUT
        [Fact]
        public void Put_ExistingGuid_Returns_Ok()
        {
            var users = _userRepository.GetAll();
            var randomUser = users.Where(u => u.IsActive == true).FirstOrDefault();

            var putResponse = _controller.Put(randomUser);

            Assert.IsType<NoContentResult>(putResponse);
        }
        [Fact]
        public void Put_NotExistingGuid_Returns_NotFound()
        {
            User user = new User()
            {
                Id = Guid.NewGuid(),
                FirstName = "Everton",
                LastName = "Medrado",
                BirthDate = RandomDate.RandomDay(),
                ShortInfo = "Hello! I'm a test user",
                RegisterDate = DateTime.Now,
                IsActive = true
            };

            var putResponse = _controller.Put(user);

            Assert.IsType<NotFoundResult>(putResponse);
        }

        [Fact]
        public void Put_NullGuid_Returns_BadRequest()
        {
            var users = _userRepository.GetAll();
            var randomUser = users.Where(u => u.IsActive == true).FirstOrDefault();

            randomUser.Id = new Guid();

            var putResponse = _controller.Put(randomUser);

            Assert.IsType<BadRequestResult>(putResponse);
        }
        //END TESTING PUT METHOD

        //START TESTING DELETE METHOD : DELETE
        [Fact]
        public void Remove_NotExistingGuid_ReturnsNotFoundResponse()
        {
            var notExistingGuid = Guid.NewGuid();

            var badResponse = _controller.Delete(notExistingGuid);

            Assert.IsType<NotFoundResult>(badResponse);
        }

        [Fact]
        public void Remove_ExistingGuidPassed_ReturnsOkResult()
        {
            var users = _userRepository.GetAll();
            var randomUser = users.Where(u => u.IsActive == true).FirstOrDefault();

            var okResponse = _controller.Delete(randomUser.Id);

            Assert.IsType<OkResult>(okResponse);
        }
        //END TESTING DELETE METHOD : DELETE
    }
}
