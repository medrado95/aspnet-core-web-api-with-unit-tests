﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using UsersAPI.Data.Repositories.Interfaces;
using UsersAPI.Entities;
using UsersAPI.Helpers;

namespace UsersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        //private readonly ETagCache _cache;
        private readonly IUserRepository _usersRepository;
        private readonly IDistributedCache _cache;

        public UsersController(IUserRepository usersRepository,
                               IDistributedCache cache)
        {
            _cache = cache;
            _usersRepository = usersRepository;

            //codigo pra popular o bd
            //for(int i = 0; i <=500; i++)
            //{
            //    var user = new User()
            //    {
            //        FirstName = "FirstName" + i.ToString(),
            //        LastName = "FirstName" + i.ToString(),
            //        BirthDate = RandomDate.RandomDay(),
            //        ShortInfo = "My lucky number is " + i.ToString(),
            //        IsActive = true,
            //        RegisterDate = DateTime.Now
            //    };
            //    _usersRepository.Add(user);
            //    _usersRepository.Commit();
            //};

        }

        // GET: api/Users
        [HttpGet]
        public ActionResult<List<User>> GetAll()
        {
            var users = _usersRepository.GetAll();
            return Ok(users);
        }

        // GET: api/Users/5
        [HttpGet("{id}", Name = "GetById")]
        public ActionResult<User> GetById(Guid id)
        {
            var cacheKey = id.ToString();
            var User = _cache.GetString(cacheKey);

            if (!string.IsNullOrEmpty(User))
            {
                return StatusCode((int)HttpStatusCode.NotModified);
            }
            else
            {
                var user = _usersRepository.GetById(id);
                if (user == null)
                {
                    return NotFound();
                }

                var userJson = JsonConvert.SerializeObject(user);
                var options = new DistributedCacheEntryOptions();
                options.SetSlidingExpiration(TimeSpan.FromSeconds(30));
                _cache.SetString(cacheKey, userJson, options);
                return Ok(userJson);
            }
        }

        // POST: api/Users
        [HttpPost]
        public IActionResult Create([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _usersRepository.Add(user);
            _usersRepository.Commit();

            return CreatedAtRoute("GetById", new { id = user.Id }, user);
        }

        // PUT: api/Users/5
        [HttpPut]
        public IActionResult Put([FromBody] User user)
        {
            if (user == null || user.Id == Guid.Empty)
            {
                return BadRequest();
            }

            var userDom = _usersRepository.GetById(user.Id);

            if (userDom == null)
            {
                return NotFound();
            }

            userDom.FirstName = user.FirstName;
            userDom.LastName = user.LastName;
            userDom.BirthDate = user.BirthDate;
            userDom.ShortInfo = user.ShortInfo;
            userDom.IsActive = user.IsActive;

            _usersRepository.Update(userDom);
            _usersRepository.Commit();
            return new NoContentResult();


        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            var userDom = _usersRepository.GetById(id);

            if (userDom == null)
            {
                return NotFound();
            }

            _usersRepository.Remove(userDom);
            _usersRepository.Commit();

            return new OkResult();
        }
    }
}
