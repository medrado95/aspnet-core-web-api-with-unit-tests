﻿using UsersAPI.Data.Repositories.Implementations.Base;
using UsersAPI.Data.Repositories.Interfaces;
using UsersAPI.Entities;

namespace UsersAPI.Data.Repositories.Implementations
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
    }
}
