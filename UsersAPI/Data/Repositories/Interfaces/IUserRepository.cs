﻿using UsersAPI.Data.Repositories.Interfaces.Base;
using UsersAPI.Entities;

namespace UsersAPI.Data.Repositories.Interfaces
{
    public interface IUserRepository : IRepositoryBase<User>
    {
    }
}
