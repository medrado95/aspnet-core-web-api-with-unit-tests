﻿using System;

namespace UsersAPI.Helpers
{
    public class RandomDate
    {
        public static DateTime RandomDay()
        {
            Random random = new Random();
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(-random.Next(range));
        }
    }
}
