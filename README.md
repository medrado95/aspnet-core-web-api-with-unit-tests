<h1>ASP .Net Core Web API with Unit Tests</h1>

This repository contains a .Net Core Web API Application which works with some Users. By now, you can GET, POST, PUT and DELETE them.




**Environment** <br>
**SO:** Windows 10 Pro <br>
**IDE:** Visual Studio Enterprise (lastest version set/18) <br>
**SERVER:** SQL Server Express <br>

 - This application uses Entity Framework Core for object related mapping, SQL connection, configuration and operations. 
 - For unit testing,
i've chose .Net Testing Tools, xUnit and Coverlet for testing coverage reports. 
- Also, i'm using Redis for a little caching service.

Provide documentation of how to setup, run and test your project (README.md).

Setup:
To run and test this project you'll need:
- Make **sure** that your .Net Core Target Framework at project's properties are exacly the same version as installed at your machine.
- Create a new SQL Server Database, and get its Connection String.
- Open Solution with your Visual Studio, and then, change connection strings at <br>
<code> appsettings.json </code> <br> and <br>
<code> Context.cs </code> <br>
- Change your connect string to your Redis Cache host at <br>
<code> Startup.cs </code>
- Go to the Package Manager Console, change your default project to UsersAPI, and run these two commands: <br>
<code> add-migration NAME OF YOUR MIGRATION </code>
and then <br>
<code> update-database </code>
- Change the connection string for Redis database server at Startup.cs
- Perform a Clean & Rebuild Solution. Should works fine.
- For testing, open Test Explorer window, right click over UsersAPITests and Run Selected Tests.




## Read All Users (Http GET)
<code> https://localhost:44369/api/users/ </code> <br>
![Semantic description of image](UsersAPI/wwwroot/img/Screenshot1.png)
## Read a Single User (Http GET)
<code> https://localhost:44369/api/users/{id} </code> <br>
![Semantic description of image](UsersAPI/wwwroot/img/Screenshot2.png)
## Create User (Http POST)
<code> https://localhost:44369/api/users/{user} </code> <br>
![Semantic description of image](UsersAPI/wwwroot/img/Screenshot3.png)
## Edit User (Http PUT)
<code> https://localhost:44369/api/users/{user} </code> <br>
![Semantic description of image](UsersAPI/wwwroot/img/Screenshot4.png)
## Delete (Http DELETE)
<code> https://localhost:44369/api/users/{id} </code> <br>
![Semantic description of image](UsersAPI/wwwroot/img/Screenshot5.png)


